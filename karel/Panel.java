package karel;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Panel extends JPanel {

	public static final int X = 50;
	public static final int Y = 30;
	public static final int BORDER = 10;

	int kx = 10;
	int ky = 10;
	int dirx = 1;
	int diry = 0;

	public boolean[][] array = new boolean[X][Y];

	public void sleep() {
		try {
			Thread.currentThread().sleep(500);
		} catch (InterruptedException ex) {
			System.out.println("wait interrupted");
		}
	}

	private int limit(int i, int max) {
		if ( i < 0 )
			return 0;
		if ( i >= max )
			return max - 1;
		return i;
	}

	public void step() {
		kx = limit(kx + dirx, X);
		ky = limit(ky + diry, Y);
		repaint();
		sleep();
	}

	public void turn() {
		int y = diry;
		diry = dirx;
		dirx = -y;
		repaint();
		sleep();
	}

	public void put() {
		array[kx][ky] = true;
		repaint();
		sleep();
	}

	public void take() {
		array[kx][ky] = false;
		repaint();
		sleep();
	}

	public boolean isBrick() {
		return array[kx][ky];
	}

	public boolean isWall() {
		int x = limit(kx + dirx, X);
		int y = limit(ky + diry, Y);
		return x == kx && y == ky;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int w = getWidth();
		int ax = w - 2 * BORDER;
		int sx = ax / X;

		int h = getHeight();
		int ay = h - 2 * BORDER;
		int sy = ay / Y;

		int s = Math.min(sx, sy);

		int offx = (ax - s * X) / 2;
		int offy = (ay - s * Y) / 2;

		int fx = offx + BORDER;
		int fy = offy + BORDER;

		g.fillRect(offx, offy, BORDER, h - 2 * offy);
		g.fillRect(w - fx, offy, BORDER, h - 2 * offy);

		g.fillRect(offx, offy, w - 2 * offx, BORDER);
		g.fillRect(offx, h - fy, w - 2 * offx, BORDER);

		for (int i = 1; i < X; i++) {
			int x = fx + i * s;
			g.drawLine(x, offy, x, h - fy);
		}

		for (int i = 1; i < Y; i++) {
			int y = fy + i * s;
			g.drawLine(offx, y, w - fx, y);
		}

		for (int i = 0; i < X; i++) {
			for (int j = 0; j < Y; j++) {
				if ( array[i][j] ) {
					int x = fx + i * s;
					int y = fy + j * s;
					g.fillRect(x, y, s, s);
				}
			}
		}

		g.setColor(Color.red);
		int x = fx + kx * s;
		int y = fy + ky * s;
		g.fillRect(x, y, s, s);
		x += s / 2;
		y += s / 2;
		g.drawLine(x, y, x + s * dirx, y + s * diry);
	}
}
