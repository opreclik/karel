package karel;

public class Karel {

	final Panel p;

	public Karel(Panel p) {
		this.p = p;
	}

	public void go() {
		p.step();
		p.put();
		p.turn();
		p.step();
	}
}
