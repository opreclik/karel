package karel;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;

public class Main {

	static Karel karel;

	public static void main(String[] args) {
		try {
			EventQueue.invokeAndWait(() -> {
				Frame f = new Frame();
				karel = new Karel(f.panel);
				f.setVisible(true);
			});
		} catch (InterruptedException | InvocationTargetException ex) {
			System.out.println("failed to create GUI" + ex);
		}
		karel.go();
	}
}
