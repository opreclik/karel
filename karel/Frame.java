package karel;

import javax.swing.JFrame;

public class Frame extends JFrame {

	Panel panel = new Panel();

	public Frame() {
		super("Karel");
		add(panel);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
